current_path=$(pwd)
docker run --name fluentdkafka --network host -p 24224:24224 -v $current_path:/fluentd/etc -e FLUENTD_CONF=fluentd.conf fluentd-kafka:1.0