current_path=$(pwd)
docker run --name kafkailg -i -t -p 9092:9092 -v $current_path/server.properties:/opt/kafka/config/server.properties -e PROP_FILE=server.properties kafka-ilg:1.0 