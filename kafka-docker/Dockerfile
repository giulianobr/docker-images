FROM openjdk:8-jre-alpine

ARG kafka_version
ARG kafka_scala_version
ARG kafka_base_url

ENV KAFKA_VERSION ${kafka_version:-0.11.0.0}
ENV KAFKA_SCALA_VERSION ${kafka_scala_version:-2.12}

ENV KAFKA_BASE_URL ${kafka_base_url:-http://apache.uib.no}
ENV KAFKA_URL "$KAFKA_BASE_URL/kafka/$KAFKA_VERSION/kafka_$KAFKA_SCALA_VERSION-$KAFKA_VERSION.tgz"
ENV KAFKA_HOME /opt/kafka

ENV KAFKA_JVM_PERFORMANCE_OPTS "-XX:MetaspaceSize=96m -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:+PrintCommandLineFlags -XX:MaxGCPauseMillis=20 -XX:InitiatingHeapOccupancyPercent=35 -XX:G1HeapRegionSize=16M -XX:MinMetaspaceFreeRatio=50 -XX:MaxMetaspaceFreeRatio=80"

ENV PROP_FILE server.properties
WORKDIR /opt

RUN apk add --no-cache wget bash

RUN wget -O - $KAFKA_URL | tar zxf - && \
    mv /opt/kafka_$KAFKA_SCALA_VERSION-$KAFKA_VERSION $KAFKA_HOME && \
    mv /opt/kafka/config/server.properties /opt/kafka/config/server.bkp.properties && \
    chown -R root:root $KAFKA_HOME

WORKDIR /opt/kafka

CMD bin/kafka-server-start.sh config/${PROP_FILE}
