#For single
#docker run -d -p 2181:2181 -p 2888:2888 -p 3888:3888 
ZOOKEEPER_IP=192.168.1.9
IP_LOCAL=192.168.1.9
docker run --name zookeeperilg -p 2181:2181 -p 2888:2888 -p 3888:3888 -d -e KAFKA_BROKER_ID=1 -e KAFKA_ADVERTISED_HOST_NAME=$IP_LOCAL -e KAFKA_ZOOKEEPER_CONNECT=$ZOOKEEPER_IP zookeeper-ilg:1.0
#For multiple, pay attention on ZOOKEEPER_* vars
#More info: https://hub.docker.com/r/digitalwonderland/zookeeper/
#docker run -d -p 2181:2181 -p 2888:2888 -p 3888:3888 -e ZOOKEEPER_ID=1 -e ZOOKEEPER_SERVER_1=172.17.8.101 -e ZOOKEEPER_SERVER_2=172.17.8.102 -e ZOOKEEPER_SERVER_3=172.17.8.103 digitalwonderland/zookeeper